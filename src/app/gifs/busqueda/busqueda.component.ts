import { ElementRef } from '@angular/core';

import { Component, ViewChild } from '@angular/core';
import { GifsService } from '../service/gifs.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent  {
  @ViewChild('text_buscar') text_buscar!:ElementRef<HTMLInputElement>

  constructor(private agregargifs: GifsService, ){

    }
  buscar(){
      const valor = this.text_buscar.nativeElement.value.toUpperCase()   
      this.agregargifs.agregarGifs(valor)
      this.text_buscar.nativeElement.value =''

      
    }
    
  



}
