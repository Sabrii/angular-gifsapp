import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ResponseType, Datum } from "../Interface/Gifs.interface";

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  constructor(private http: HttpClient) {
    this._colectionGifs = JSON.parse(localStorage.getItem('historial')!) || []
    this.respuesta = JSON.parse(localStorage.getItem('resultados')!) || []
  }

  private _colectionGifs: string[] = []
  public respuesta: Datum[] = []

  agregarGifs(gif: string) {
    if (gif.trim().length === 0) return
    if (!this._colectionGifs.includes(gif)) {
      this._colectionGifs.unshift(gif)
      console.log(this._colectionGifs)
      localStorage.setItem('historial', JSON.stringify(this._colectionGifs))
    }
    const params = new HttpParams()
      .set('api_key', 'lFVBQwy8pcvh1PCEjod9NEAJMGE9GMWI')
      .set('limit', '10')
      .set ('offset', '0')
      .set ('q', gif)
      
    this.http.get<ResponseType>(`https://api.giphy.com/v1/gifs/search`,{params})
      .subscribe(resp => {
        console.log(resp.data)
        this.respuesta = resp.data
        localStorage.setItem('resultados', JSON.stringify(this.respuesta))

      })
  }

  get returnColectionGifs() {
    return [...this._colectionGifs]
  }




}
