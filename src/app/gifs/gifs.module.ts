import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { ResultadoComponent } from './resultado/resultado.component';



@NgModule({
  declarations: [BusquedaComponent, ResultadoComponent],
  imports: [
    CommonModule
  ],
  exports:[
    BusquedaComponent,
    ResultadoComponent
  ]
})
export class GifsModule { }
