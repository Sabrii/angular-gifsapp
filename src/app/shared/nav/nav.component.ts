import { Component, OnInit } from '@angular/core';
import { GifsService } from 'src/app/gifs/service/gifs.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent  {
  constructor(private historialGifs:GifsService ){
  }
  
  get listadoGifs(){
    return this.historialGifs.returnColectionGifs  

  }
  buscar(gif:string){
    return this.historialGifs.agregarGifs(gif)

  }
}
